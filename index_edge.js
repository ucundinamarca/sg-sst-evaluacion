/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var im='images/',
        aud='media/',
        vid='media/',
        js='js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "6.0.0",
                minimumCompatibleVersion: "5.0.0",
                build: "6.0.0.400",
                scaleToFit: "none",
                centerStage: "both",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'background',
                            type: 'rect',
                            rect: ['10px', '7px', '1022px', '638px', 'auto', 'auto'],
                            fill: ["rgba(244,244,244,1.00)"],
                            stroke: [1,"rgba(0,0,0,1)","solid"]
                        },
                        {
                            id: 'main',
                            type: 'group',
                            rect: ['23px', '17px', '1000', '476', 'auto', 'auto'],
                            c: [
                            {
                                id: 'Recurso_25',
                                type: 'image',
                                rect: ['19px', '13px', '566px', '55px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                            },
                            {
                                id: 'icon1',
                                type: 'image',
                                rect: ['85px', '172px', '238px', '304px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_23.png",'0px','0px'],
                                userClass: "pointer item"
                            },
                            {
                                id: 'icon2',
                                type: 'image',
                                rect: ['399px', '172px', '238px', '304px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_24.png",'0px','0px'],
                                userClass: "pointer item"
                            },
                            {
                                id: 'icon3',
                                type: 'image',
                                rect: ['703px', '172px', '238px', '304px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_22.png",'0px','0px'],
                                userClass: "pointer item"
                            },
                            {
                                id: 'Recurso_20',
                                type: 'image',
                                rect: ['650px', '377px', '46px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_20.png",'0px','0px'],
                                userClass: "item"
                            },
                            {
                                id: 'Recurso_21',
                                type: 'image',
                                rect: ['344px', '377px', '46px', '46px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_21.png",'0px','0px'],
                                userClass: "item"
                            }]
                        },
                        {
                            id: 'container-question',
                            type: 'group',
                            rect: ['23px', '17px', '1000', '539', 'auto', 'auto'],
                            c: [
                            {
                                id: 'box_container',
                                type: 'rect',
                                rect: ['14px', '107px', '504px', '351px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                userClass: "box_shadow"
                            },
                            {
                                id: 'box_container2',
                                type: 'rect',
                                rect: ['14px', '107px', '962px', '351px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1.00)"],
                                stroke: [0,"rgba(0,0,0,1)","none"],
                                userClass: "box_shadow"
                            },
                            {
                                id: 'options',
                                type: 'rect',
                                rect: ['20px', '506px', '184px', '64px', 'auto', 'auto'],
                                fill: ["rgba(192,192,192,0.00)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'image-1',
                                type: 'image',
                                rect: ['534px', '237px', '442px', '302px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_28.png",'0px','0px'],
                                userClass: "images"
                            },
                            {
                                id: 'image-2',
                                type: 'image',
                                rect: ['553px', '167px', '404px', '372px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_46.png",'0px','0px'],
                                userClass: "images"
                            },
                            {
                                id: 'image-3',
                                type: 'image',
                                rect: ['569px', '165px', '388px', '374px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"Recurso_62.png",'0px','0px'],
                                userClass: "images"
                            },
                            {
                                id: 'game1',
                                type: 'group',
                                rect: ['14px', '107px', '962', '432', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'text-g-1',
                                    type: 'rect',
                                    rect: ['19px', '11px', '696px', '73px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'i1-d-5',
                                    type: 'image',
                                    rect: ['398px', '209px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_39.png",'0px','0px']
                                },
                                {
                                    id: 'i1-d-4',
                                    type: 'image',
                                    rect: ['97px', '215px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_40.png",'0px','0px']
                                },
                                {
                                    id: 'i1-d-3',
                                    type: 'image',
                                    rect: ['507px', '109px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_41.png",'0px','0px']
                                },
                                {
                                    id: 'i1-d-2',
                                    type: 'image',
                                    rect: ['251px', '109px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_42.png",'0px','0px']
                                },
                                {
                                    id: 'i1-d-1',
                                    type: 'image',
                                    rect: ['-2px', '109px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_43.png",'0px','0px']
                                },
                                {
                                    id: 'dropp-g-1',
                                    type: 'rect',
                                    rect: ['49px', '122px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: 'dropp-g-2',
                                    type: 'rect',
                                    rect: ['306px', '122px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: 'dropp-g-3',
                                    type: 'rect',
                                    rect: ['560px', '122px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: 'dropp-g-4',
                                    type: 'rect',
                                    rect: ['165px', '227px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: 'dropp-g-5',
                                    type: 'rect',
                                    rect: ['463px', '227px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: 'dragg-g-1',
                                    type: 'rect',
                                    rect: ['772px', '10px', '180px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: 'dragg-g-2',
                                    type: 'rect',
                                    rect: ['772px', '76px', '180px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: 'dragg-g-3',
                                    type: 'rect',
                                    rect: ['772px', '142px', '180px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: 'dragg-g-4',
                                    type: 'rect',
                                    rect: ['772px', '208px', '180px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: 'dragg-g-5',
                                    type: 'rect',
                                    rect: ['772px', '273px', '180px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                }]
                            },
                            {
                                id: 'game2',
                                type: 'group',
                                rect: ['14px', '107px', '962', '432', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'text-g-2',
                                    type: 'rect',
                                    rect: ['19px', '11px', '696px', '73px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'i2-d-3',
                                    type: 'image',
                                    rect: ['507px', '147px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_56.png",'0px','0px']
                                },
                                {
                                    id: 'i2-d-2',
                                    type: 'image',
                                    rect: ['251px', '147px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_57.png",'0px','0px']
                                },
                                {
                                    id: 'i2-d-1',
                                    type: 'image',
                                    rect: ['-2px', '147px', '52px', '107px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_58.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_55',
                                    type: 'image',
                                    rect: ['659px', '117px', '100px', '41px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_55.png",'0px','0px']
                                },
                                {
                                    id: 'Recurso_54',
                                    type: 'image',
                                    rect: ['659px', '226px', '100px', '41px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_54.png",'0px','0px']
                                },
                                {
                                    id: '_dropp-g-1',
                                    type: 'rect',
                                    rect: ['49px', '160px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: '_dropp-g-2',
                                    type: 'rect',
                                    rect: ['306px', '160px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: '_dropp-g-3',
                                    type: 'rect',
                                    rect: ['560px', '160px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: '_dropp-g-4',
                                    type: 'rect',
                                    rect: ['763px', '95px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: '_dropp-g-5',
                                    type: 'rect',
                                    rect: ['763px', '226px', '180px', '66px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dropp"
                                },
                                {
                                    id: '_dragg-g-1',
                                    type: 'rect',
                                    rect: ['217px', '399px', '150px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: '_dragg-g-2',
                                    type: 'rect',
                                    rect: ['366px', '399px', '150px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: '_dragg-g-3',
                                    type: 'rect',
                                    rect: ['515px', '399px', '150px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: '_dragg-g-4',
                                    type: 'rect',
                                    rect: ['664px', '399px', '150px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                },
                                {
                                    id: '_dragg-g-5',
                                    type: 'rect',
                                    rect: ['813px', '399px', '150px', '66px', 'auto', 'auto'],
                                    cursor: 'move',
                                    fill: ["rgba(255,255,255,1.00)",[270,[['rgba(38,50,56,1.00)',100],['rgba(255,255,255,1.00)',100]]]],
                                    stroke: [0,"rgb(0, 0, 0)","none"],
                                    userClass: "dragg"
                                }]
                            },
                            {
                                id: 'header',
                                type: 'group',
                                rect: ['0px', '0px', '1000', '75', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'Recurso_252',
                                    type: 'image',
                                    rect: ['19px', '13px', '566px', '55px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_25.png",'0px','0px']
                                }]
                            }]
                        },
                        {
                            id: 'alert',
                            type: 'group',
                            rect: ['0', '0', '1044', '667', 'auto', 'auto'],
                            c: [
                            {
                                id: 'alert-background',
                                type: 'rect',
                                rect: ['0px', '0px', '1044px', '667px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0.56)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'alert-box',
                                type: 'group',
                                rect: ['188', '166', '624', '302', 'auto', 'auto'],
                                c: [
                                {
                                    id: 'box-alert',
                                    type: 'rect',
                                    rect: ['0px', '0px', '624px', '302px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'header-alert',
                                    type: 'rect',
                                    rect: ['0px', '0px', '622px', '34px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1.00)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'msg-alert',
                                    type: 'rect',
                                    rect: ['30px', '53px', '566px', '219px', 'auto', 'auto'],
                                    fill: ["rgba(255,255,255,1)"],
                                    stroke: [0,"rgb(0, 0, 0)","none"]
                                },
                                {
                                    id: 'close',
                                    type: 'image',
                                    rect: ['598px', '-17px', '50px', '51px', 'auto', 'auto'],
                                    fill: ["rgba(0,0,0,0)",im+"Recurso_33.png",'0px','0px'],
                                    userClass: "pointer"
                                }]
                            }]
                        },
                        {
                            id: 'preload',
                            type: 'group',
                            rect: ['23px', '17px', '1000px', '617px', 'auto', 'auto'],
                            c: [
                            {
                                id: 'fondo-preload',
                                type: 'rect',
                                rect: ['0px', '0px', '1000px', '617px', 'auto', 'auto'],
                                fill: ["rgba(255,255,255,1)"],
                                stroke: [0,"rgb(0, 0, 0)","none"]
                            },
                            {
                                id: 'logo',
                                type: 'image',
                                rect: ['462px', '270px', '100px', '100px', 'auto', 'auto'],
                                fill: ["rgba(0,0,0,0)",im+"logo.svg",'0px','0px']
                            }]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: ['null', 'null', '1044px', '660px', 'auto', 'auto'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 0,
                    autoPlay: true,
                    data: [
                        [
                            "eid7",
                            "width",
                            0,
                            0,
                            "linear",
                            "${background}",
                            '1022px',
                            '1022px'
                        ],
                        [
                            "eid10",
                            "top",
                            0,
                            0,
                            "linear",
                            "${background}",
                            '7px',
                            '7px'
                        ],
                        [
                            "eid8",
                            "height",
                            0,
                            0,
                            "linear",
                            "${background}",
                            '638px',
                            '638px'
                        ],
                        [
                            "eid1",
                            "background-image",
                            0,
                            0,
                            "linear",
                            "${background}",
                            [270,[['rgba(255,255,255,0.00)',0],['rgba(255,255,255,0.00)',100]]],
                            [270,[['rgba(255,255,255,0.00)',0],['rgba(255,255,255,0.00)',100]]]
                        ],
                        [
                            "eid9",
                            "left",
                            0,
                            0,
                            "linear",
                            "${background}",
                            '10px',
                            '10px'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load("index_edgeActions.js");
})("EDGE-103770202");

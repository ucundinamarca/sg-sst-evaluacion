var ST = "#Stage_";
var S = "Stage_";
var NOTE_SCALE = 100;
var NOTE_QUESTION = 9;
var INDEX_QUESTION = 0;
//preguntas 3 y 4 son arreglos dado que son las respuestas del dragg and dropp
var ANSWER_QUESTION = ['','',['','','','',''],'','',['','','','',''],'','',''];
ivo.info({
    title: "Evaluación sistema de gestión de la seguridad y salud en el trabajo SG-SST",
    autor: "Edilson Laverde Molina",
    date: "",
    email: "edilsonlaverde_182@hotmail.com",
    icon: 'https://www.ucundinamarca.edu.co/images/iconos/favicon_verde_1.png',
    facebook: "https://www.facebook.com/edilson.laverde.molina"
});
var audios = [
{
    url: "sonidos/click.mp3",
    name: "clic"
},
{
    url: "sonidos/fail.mp3",
    name: "fail"
},
{
    url: "sonidos/good.mp3",
    name: "good"
}
];
var scenarios = [
    {
        n:1,
        id: "textQuestion1",
        boxHTML: `
        <p>
            <b>Instrucción: </b>Lea el siguiente caso y realice la actividad de complemento: Escriba en el espacio vacío las palabras correspondientes a la respuesta correcta.
        </p>
        <p>
            Juan y Pedro son funcionarios de la universidad pertenecientes al área de mantenimiento y logística. El día 3 de septiembre del 2019, poco antes del mediodía, se encontraban cambiando bombillos dañados en el edificio principal de la universidad. Dada la altura de los focos, debieron hacer uso de una escalera de aluminio. Pedro sube un par de escalones, mientras trata de rotar con vigorosidad un bombillo que presentaba resistencia, resbala desde la escalera y cae en el piso sobre su costado izquierdo, lastimándose el codo de dicho brazo. 
        </p>
        <p>
            <b>Lo que acaba de suceder debe catalogarse como: </b>
        </p>
        `,
        text: ["accidente de trabajo"],
        feedback: "Lo ocurrido es un accidente de trabajo porque se trató de un suceso no deseado que sobrevino por causa u ocasión de la realización del trabajo, que además produjo en el trabajador una lesión orgánica. ",
        class_items: "red",
        idImage: 'image-1',
        page:1,
        nameContainer: "container-textQuestion",
    },
    {
        n:2,
        id: "question1",
        boxHTML: `
        <p><b>Instrucción:</b> Realice el siguiente cuestionario de selección múltiple, con única respuesta. </p>
        <p>Viendo lo que le sucedió a Pedro, Juan quiere ayudarlo y procede a hacerlo. Revisa visualmente la situación de Pedro y le pregunta ¿cómo se siente? Una vez que comprende la situación, procede a reportar el accidente de trabajo y se comunica con:</p>`,
        options: ['El centro médico más cercano.','La oficina de SST.','El médico que le indicó la oficina de SST.','Samuel, su mejor amigo, porque es enfermero.'],
        correct: [0, 1, 0, 0],
        feedback: "El primer paso para reportar un accidente de trabajo siempre será comunicarse en el momento del evento con los teléfonos fijos de la oficina de SST.",
        class_items: "red",
        idImage: 'image-1',
        page:2,
        nameContainer: 'container-question'
    },
    {
        n:3,
        id: "draggable1",
        boxHTML: `
        <p><b>Instrucción:</b> Realice la siguiente actividad de arrastre.</p>
        <p>Dado que entrar en contacto con la oficina de SST es apenas el primer paso en el proceso para reportar un accidente de trabajo, el proceso completo y organizado según su secuencialidad es:</p>`,
        options: [
            {
                id: ST+"dragg-g-1",
                text: "Comunicarse en el momento del evento a los teléfonos fijos de oficina SST (828-1483) ex. 224-260",
            },{
                id: ST+"dragg-g-2",
                text: "Dirigirse al centro médico indicado por la oficina de SST",
            },{
                id: ST+"dragg-g-3",
                text: "Reportar el at en del formato (código atr 218)",
            },{
                id: ST+"dragg-g-4",
                text: "Informar al área de SST el estado de salud",
            },{
                id: ST+"dragg-g-5",
                text: "Investigación de accidente de trabajo (no mayor a 15 días)",
            },
        ],
        correct: [0, 1, 0, 0],
        feedback: "Recuerde que, para continuar con el reporte del accidente de trabajo, Juan debe proveer la siguiente información a la oficina de SST: Sede, Fecha del siniestro, Horario de trabajo, Parte del cuerpo afectada, Descripción detallada del accidente, Cédula y teléfono de Juan, por ser testigo.",
        class_items: "red",
        idImage: 'image-1',
        page:3,
        nameContainer: 'container-dragg-and-dropp-1'
    },
    {
        n:4,
        id: "textQuestion1",
        boxHTML: `
        <p>
            <b>Instrucción: </b>Lea el siguiente caso y realice la actividad de complemento: Escriba en el espacio vacío las palabras correspondientes a la respuesta correcta. 
        </p>
        <p>
            Claudia y Sara trabajan en el área administrativa de la universidad. Un día Claudia se desplazó al cubículo de Sara porque necesitaba solicitarle un informe que estaba pendiente. Al llegar, saluda a Sara y le pregunta por el informe, ella le responde que está a punto de imprimirlo, pero la impresora está desconectada y no alcanza a conectarla por sí sola. Claudia se ofrece para ayudarle a conectar, pero nota que le tomacorriente presenta manchas negras y las perforaciones de acceso un poco derretidas. 
        </p>
        <p>
            <b>En el marco del Sistema de Gestión de Seguridad y Salud en el Trabajo, lo que Claudia acaba de encontrar es: </b>
        </p>
        `,
        text: ["condición insegura", "una condición insegura","condicion insegura", "una condicion insegura"],
        feedback: 'Una <b>condición insegura</b> es una condición física insatisfactoria que existe en un entorno de trabajo inmediatamente antes de ocurrir un accidente, y que fue significativa para iniciar el evento. Es un peligro que tiene el potencial de causar daños a la propiedad, lesiones o la muerte a un trabajador, si no se corrige adecuadamente.',
        class_items: "green",
        idImage: 'image-2',
        page:1,
        nameContainer: "container-textQuestion",
    },
    {
        n:5,
        id: "question1",
        boxHTML: `
        <p><b>Instrucción:</b> Realice el siguiente cuestionario de selección múltiple, con única respuesta. </p>
        <p>Según la definición de una condición insegura, uno de estos casos no es un ejemplo de condición insegura en el trabajo:</p>`,
        options: ['Suciedad y desorden en el área de trabajo.','Enfermedad contraída por exposición a factores de riesgo.','Cables energizados, sueltos.','Herramientas rotas o deformes.'],
        correct: [0, 1, 0, 0],
        feedback: "No puede tratarse de una enfermedad porque la condición insegura hace referencia a una situación previa, anterior a que ocurra un accidente o se origine el evento. La enfermedad, en contraste, hace referencia a efectos.",
        class_items: "green",
        idImage: 'image-2',
        page:2,
        nameContainer: 'container-question'
    },
    {
        n:3,
        id: "draggable1",
        boxHTML: `
        <p><b>Instrucción:</b> Realice la siguiente actividad de arrastre.</p>
        <p><b>¿Cómo reportar una condición insegura?</b><br><b>Estos son los pasos por seguir:</b></p>
        
        `,
        options: [
            {
                id: ST+"_dragg-g-1",
                text: "Tomar una foto con el celular o  describir por escrito y detalladamente la condición insegura",
            },{
                id: ST+"_dragg-g-2",
                text: "Comunicarse con la Oficina de Seguridad y Salud en el trabajo",
            },{
                id: ST+"_dragg-g-3",
                text: "Canales de comunicación de la Oficina de SST",
            },{
                id: ST+"_dragg-g-4",
                text: "Tel.: 8281483 (Ext.:240 y 260)",
            },{
                id: ST+"_dragg-g-5",
                text: "E-mail: saludocupacional @ucundinamarca.edu.co",
            },
        ],
        correct: [0, 1, 0, 0],
        feedback: "Asegúrate de seguir cada uno de los pasos para realizar de manera adecuada el reporte de una condición insegura.",
        class_items: "green",
        idImage: 'image-2',
        page:3,
        nameContainer: 'container-dragg-and-dropp-2'
    },
    {
        n:7,
        id: "textQuestion3",
        boxHTML: `
        <p>
            <b>Instrucción: </b>Lea el siguiente caso y realice la actividad de complemento: Escriba en el espacio vacío las palabras correspondientes a la respuesta correcta. 
        </p>
        <p>
            Samuel es un diseñador que trabaja en el área de producción de la universidad. Todos los días y por largos periodos de tiempo Samuel utiliza el “mouse” como su herramienta principal de trabajo, esto con la mano derecha dado que es su extremidad dominante. De un tiempo para acá Samuel se tornó torpe para agarrar objetos con la mano derecha, después comenzó a sentir hormigueo en el pulgar de la misma mano y, en los días más recientes, un dolor intenso que se extendía hasta el codo. Debido a su situación, acudió al médico y este le diagnóstico síndrome de túnel carpiano. 
        </p>
        <p>
            <b>La situación de Samuel se puede identificar como una</b>
        </p>
        `,
        text: ["enfermedad laboral"],
        feedback: 'Se trata de una enfermedad laboral contraída como resultado de la exposición a factores de riesgo inherentes a la actividad laboral o del medio en el que el trabajador se ha visto obligado a trabajar. ',
        class_items: "yellow",
        idImage: 'image-3',
        page:1,
        nameContainer: "container-textQuestion",
    },
    {
        n:8,
        id: "question3",
        boxHTML: `
        <p><b>Instrucción:</b> Realice la siguiente actividad de verdadero o falso. </p>
        <p>¿Hay una tipificación única y exclusiva de cuáles son las enfermedades que pueden categorizarse como enfermedades laborales?</p>`,
        options: ['Verdadero','Falso'],
        correct: [0, 1],
        feedback: "No, no existe tal tipificación única de las enfermedades susceptibles de ser catalogadas como laborales. El Gobierno Nacional determinará en forma periódica las enfermedades que se consideran como laborales y en los casos en que la enfermedad no figure en la tabla de enfermedades laborales, pero se demuestre la relación de causalidad con los factores de riesgo ocupacionales, será reconocida como enfermedad laboral, conforme lo establecido en las formas legales vigentes. (Ley 1562 de 2012 Articulo 3).",
        class_items: "yellow",
        idImage: 'image-3',
        page:2,
        nameContainer: 'container-question'
    },
    {
        n:9,
        id: "question3",
        boxHTML: `
        <p><b>Instrucción:</b> Realice la siguiente actividad de verdadero o falso. </p>
        <p>Según la oficina de SST de la universidad, solo hay una manera válida para reportar una enfermedad laboral. ¿Cuáles son los canales para hacerlo?</p>`,
        options: [
            'Comunicarse al teléfono 8281483 (ext.: 240 o 260) o escribir al correo electrónico saludocupacional@ucundinamarca.edu.co',
            'Reportar el incidente al jefe inmediato mediante un correo a su secretaria.',
            'Dirigirse inmediatamente al centro de salud más cercano y reportarlo con le médico de urgencias. ',
            'Comunicarse directamente con la ARL y que ella entre en contacto con la oficina de SST en la universidad.'
        ],
        correct: [1, 0, 0 ,0],
        feedback: "Asegúrate de conocer cada uno de los canales de comunicación para reportar una enfermedad laboral.",
        class_items: "yellow",
        idImage: 'image-3',
        page:3,
        nameContainer: 'container-question'
    }

]
var mainActive = true;
function main(sym) {
    var t = null;
    udec = ivo.structure({
        created: function () {
            t = this;
            t.interpolar_position([S+"dragg-g-1",S+"dragg-g-2",S+"dragg-g-3",S+"dragg-g-4",S+"dragg-g-5"]);
            t.interpolar_position([S+"_dragg-g-1",S+"_dragg-g-2",S+"_dragg-g-3",S+"_dragg-g-4",S+"_dragg-g-5"]);
            ivo(ST + "icon2").addClass("visited");
            ivo(ST + "icon3").addClass("visited");
            ivo(ST + "game1").hide();
            ivo(ST + "game2").hide();
            t.animation();
            
            //precarga audios//
            ivo.load_audio(audios, onComplete = function () {
                ivo(ST + "preload").hide();
                stage1.play();
                t.events();
            });
        },
        methods: {
            interpolar_position: function(divs){
                // Crear un array para almacenar las posiciones de los divs
                const posiciones = [];

                // Iterar sobre los divs para obtener sus posiciones
                divs.forEach(div => {
                posiciones.push({
                    top:  document.getElementById(div).style.top,
                    left:  document.getElementById(div).style.left
                });
                });

                // Desordenar el array de posiciones
                posiciones.sort(() => Math.random() - 0.5);

                // Iterar sobre los divs y asignarles nuevas posiciones
                divs.forEach((div, index) => {
                   
                    document.getElementById(div).style.top = posiciones[index].top;
                    document.getElementById(div).style.left = posiciones[index].left;
                });
            },
            analitys_note: function () {
                var points = 0;
                var value_points = NOTE_SCALE / NOTE_QUESTION;
                //recorresmos los escenarios los comparamos con las respuestas del usuario
                for (var i = 0; i < scenarios.length; i++) {
                    if (scenarios[i].nameContainer == "container-question") {
                        if (ANSWER_QUESTION[i] == '1') {
                            points += value_points;
                        }
                    }
                    if (scenarios[i].nameContainer == "container-textQuestion") {
                        //for para recorrer las respuestas del usuario//
                        for (var j=0; j<scenarios[i].text.length; j++){
                            if (ANSWER_QUESTION[i].toLowerCase() == scenarios[i].text[j].toLowerCase()) {
                                points += value_points;
                            }
                        }

                    }
                    if (scenarios[i].nameContainer == "container-dragg-and-dropp-1" || scenarios[i].nameContainer == "container-dragg-and-dropp-2") {
                        //recorremos las opaciones//
                        let sub_point = scenarios[i].options.length;
                        for (var j = 0; j < scenarios[i].options.length; j++) {
                            if (scenarios[i].options[j].text == ANSWER_QUESTION[i][j]) {
                                sub_point--;
                            }
                        }
                        if (sub_point == 0) {
                            points += value_points;
                        }
                    }
                }
                let msg_winner = '¡Excelente! Felicidades, ha aprobado el curso, se evidencia el fortalecimiento de su aprendizaje y el manejo de conceptos en relación al SGSST.';
                let msg_loser = '¡Ánimo! Siga intentándolo, recuerde la importancia de revisar los recursos educativos del curso, para fortalecer su aprendizaje en relación al SGSST. Lo invitamos a realiza nuevamente las actividades.';
                if (points >= value_points) { //paso la prueba
                    ivo(ST + "msg-alert").html('<p>'+msg_winner+'</p>');
                
                } else { //no paso la prueba
                    ivo(ST + "msg-alert").html('<p>'+msg_loser+'</p>');
                }
                Scorm_mx = new MX_SCORM(false);
                console.log("Nombre: " + Scorm_mx.info_user().name + " Id: " + Scorm_mx.info_user().id+ " Nota: "+points);
                Scorm_mx.set_score(points);

                alert.timeScale(1).play();   
            },
            showAlert: function () {
                ivo(ST + "header-alert").addClass(scenarios[INDEX_QUESTION].class_items);
                ivo(ST + "msg-alert").html('<h1 style="width:100%; text-align:center;">Retroalimentación</h1><p>'+scenarios[INDEX_QUESTION].feedback+"</p>");
                alert.timeScale(1).play();
            },
            gameStart: function (group) {
                let _this = this;
                $(".dragg").draggable();
                $(".dropp").droppable({
                    drop: function (event, ui) {
                        let indexId = parseInt($(this).attr("id").split(group)[1]) - 1;
                        let textDragg = ui.draggable.text();
                        //almezanar el texto del draggable en el droppable ANWER_QUESTION
                        ANSWER_QUESTION[INDEX_QUESTION][indexId] = textDragg;
                        //analizamos si contesto bien//
                        if (ANSWER_QUESTION[INDEX_QUESTION][indexId] == scenarios[INDEX_QUESTION].options[indexId].text) { 
                            ivo.play("good");
                        } else {
                            ivo.play("fail");
                        }
                        // hacemos visible la accion pasado el draggable al droppable
                        $(this).html(textDragg);
                        //ocultamo el draggable ya no lo necesitamos
                        ui.draggable.hide();
                        //analizamos si ya se llenaron los 5 elementos del array for
                        let n = 5;
                        for (var i = 0; i < scenarios[INDEX_QUESTION].options.length; i++) {
                            if (ANSWER_QUESTION[INDEX_QUESTION][i] == "") {
                                n--;
                            }
                        }
                        if (n == 5) {
                            _this.showAlert();
                            INDEX_QUESTION++;
                            ivo(ST+ "game1").hide();
                            ivo(ST+ "game2").hide();
                            _this.set_info(INDEX_QUESTION);
                            
                        }
                    }
                });
                
            },
            setIndicators: function (question) {
                ivo(ST + "options").html(`
                    <ul class="list">
                        <li class="pointer item_circle ${question.page==1 ? question.class_items : ''}">1</li>
                        <li class="pointer item_circle ${question.page==2 ? question.class_items : ''}">2</li>
                        <li class="pointer item_circle ${question.page==3 ? question.class_items : ''}">3</li>
                    </ul>
                `);
            },
            setImage: function (question) { 
                ivo(".images").hide();
                
                if (question.nameContainer == "container-dragg-and-dropp-1" || question.nameContainer == "container-dragg-and-dropp-2") { 

                }else{
                    ivo(ST + question.idImage).show();
                }
            },
            setQuestionText: function (question) { 
                var _this = this;
                ivo(ST + "box_container").html(question.boxHTML);
                ivo(ST + "box_container").append(`
                    <p>
                        <input class="input ${question.class_items}" placeholder="Escriba aquí su respuesta y después presione enter para enviar" name="text" id="text">
                    </p>
                `);
                var input = document.getElementById("text");
                input.addEventListener("keyup", function(event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        //se almacena valor del input en el array de respuestas
                        ANSWER_QUESTION[INDEX_QUESTION] = input.value;
                        //validamos la respuesta//
                        let mal=true;
                        for (var i=0; i<scenarios[INDEX_QUESTION].text.length; i++){
                            if (ANSWER_QUESTION[INDEX_QUESTION].toLowerCase() === scenarios[INDEX_QUESTION].text[i].toLowerCase()) {
                                mal=false;
                            }
                        }
                        if (mal){
                            ivo.play("fail");
                        }else{
                            ivo.play("good");
                        }
                        _this.showAlert();
                        //se incrementa el indice de la pregunta
                        INDEX_QUESTION++;
                        _this.set_info(INDEX_QUESTION);
                        
                        

                    }
                });
            },
            setQuestionDaggandDrop: function (question) { 
                if (question.nameContainer == "container-dragg-and-dropp-1") {
                    ivo(ST + "text-g-1").html(question.boxHTML);
                    this.gameStart("Stage_dropp-g-");
                    ivo(ST+ "game1").show();
                    
                }
                if (question.nameContainer == "container-dragg-and-dropp-2") {
                    ivo(ST + "text-g-2").html(question.boxHTML);
                    this.gameStart("Stage__dropp-g-");
                    ivo(ST+ "game2").show();
                    
                }
                //seteamos los textos de las opciones draggables//
                for (var i = 0; i < question.options.length; i++) {
                    ivo(question.options[i].id).html(question.options[i].text);
                }
                
            },
            setQuestion: function (question) {
                ivo(ST + "box_container").html(question.boxHTML);
                let optionsHTML = '<ul class="question"></ul>';
                ivo(ST + "box_container").append(optionsHTML);
                //agregar opciones
                let optionHTML = '';
                indicadores = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
                for (let i = 0; i < question.options.length; i++) {
                    let option  = question.options[i];
                    let correct = question.correct[i];
                    optionHTML += `<li data-class="${question.class_items}" class="option" id="o${i}" data-correct="${correct}"><span class="item_circle ${question.class_items}">${indicadores[i]}</span><div>${option}</div></li>`;
                }
                ivo(ST + "box_container ul.question").html(optionHTML);
                //efecto quitar class
                let _this = this;
                ivo(".option").on("click", function () {
                    ANSWER_QUESTION[INDEX_QUESTION] = ivo(this).attr("data-correct");
                    //se incrementa el indice de la pregunta
                    ivo.play("clic");
                    if (ANSWER_QUESTION[INDEX_QUESTION] == '1') {
                        ivo.play("good");
                    } else { 
                        ivo.play("fail");
                    }
                    _this.showAlert();
                    INDEX_QUESTION++;
                    _this.set_info(INDEX_QUESTION);
                }).on("mouseover", function () {
                    let id = ivo(this).attr("id");
                    let classname = ivo(this).attr("data-class");
                    ivo("#"+id+ " span").removeClass(classname);
                })
                .on("mouseout", function () {
                    let id = ivo(this).attr("id");
                    let classname = ivo(this).attr("data-class");
                    ivo("#"+id+ " span").addClass(classname);
                });
            },
            set_info: function (index) {
                let info = {};
                if (index == 3 && !mainActive) {
                    //menu segundo elemento//
                    mainActive = true;
                    ivo(ST + "icon2").removeClass("visited");
                    stage2.reverse();
                    return;
                }
                if (index == 6 && !mainActive) {
                    //menu tercero elemento//
                    mainActive = true;
                    ivo(ST + "icon3").removeClass("visited");
                    stage2.reverse();
                    return;
                }
                console.log(index);
                console.log(mainActive);
                if (index == 9 && !mainActive) {
                    //menu tercero elemento//
                    mainActive = true;
                    stage2.reverse();
                    this.analitys_note();
                    return;
                }
                info = scenarios[index];
                ivo(".box_shadow").hide();
                if (info.nameContainer == "container-question") {
                    ivo(ST + "box_container").show();
                    t.setQuestion(info);
                }
                if (info.nameContainer == "container-textQuestion") {
                    t.setQuestionText(info);
                    ivo(ST + "box_container").show();
                }
                if (info.nameContainer == "container-dragg-and-dropp-1") {
                    t.setQuestionDaggandDrop(info);
                    ivo(ST + "box_container2").show();
                }
                if (info.nameContainer == "container-dragg-and-dropp-2") {
                    t.setQuestionDaggandDrop(info);
                    ivo(ST + "box_container2").show();
                }
                this.setIndicators(info);
                this.setImage(info);
                mainActive = false;
            },
            events: function () {
                var t = this;
                ivo(ST+"close").on("click", function () {
                    
                    
                    alert.timeScale(3).reverse();
                    ivo.play("clic");

                });
                ivo(".item").on("click", function () {    
                    let id = ivo(this).attr("id").split(S + "icon")[1];
                    if (INDEX_QUESTION == 0 && id == 1) {
                        t.set_info(INDEX_QUESTION);
                        stage2.timeScale(1).play();
                        ivo.play("clic");
                    }
                    if (INDEX_QUESTION == 3 && id == 2) {
                        t.set_info(INDEX_QUESTION);
                        stage2.timeScale(1).play();
                        ivo.play("clic");
                    }
                    if (INDEX_QUESTION == 6 && id == 3) {
                        t.set_info(INDEX_QUESTION);
                        stage2.timeScale(1).play();
                        ivo.play("clic");
                    }
                })
                .on("mouseover", function () {
                    ivo(this).css("opacity", .7);
                    let id = ivo(this).attr("id").split(S + "icon")[1];
                    if ((INDEX_QUESTION==0 && id == 1) || (INDEX_QUESTION == 3 && id == 2) || (INDEX_QUESTION == 6 && id == 3)) {
                        ivo(this).css("cursor", "pointer");
                    } else {
                        ivo(this).css("cursor", "no-drop");
                    }
                })
                .on("mouseout", function () {
                    ivo(this).css("opacity",1);    
                });
            },
            animation: function () {
                stage1 = new TimelineMax();
                stage1.append(TweenMax.from(ST + "main", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "Recurso_25", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.from(ST + "credit", .8, {x: 1300, opacity: 0}), 0);
                stage1.append(TweenMax.staggerFrom(".item", .4, {x: 1000, opacity: 0, scaleY: 6, ease: Elastic.easeOut.config(0.3, 0.4)}, .2), 0);
                stage1.stop();

                alert = new TimelineMax();
                alert.append(TweenMax.from(ST + "alert", .8, {x: 1300, opacity: 0}), 0);
                alert.append(TweenMax.from(ST + "alert-box", .8, {y: 1300, opacity: 0}), 0);
                alert.append(TweenMax.from(ST + "close", .8, {scale: 0, rotation: 900}), 0);
                alert.stop();

                stage2 = new TimelineMax({
                    onStart: function () {
                        stage1.timeScale(7).reverse();
                    },
                    onReverseComplete: function () {
                        stage1.timeScale(1).play();
                    }
                });
                stage2.append(TweenMax.from(ST + "container-question", .8, { y: 1300, opacity: 0 }), 0);
                stage2.stop();
            }
        }
    });
}
/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
          yepnope({
                  	nope:[
                  		'css/udec.css',
                  		'css/ui-udec.css',
                        'js/SCORM_API_wrapper.js',
                        'js/MX_SCORM.js',
                        'js/sweetalert.min.js',
                        'js/jquery-3.3.1.min.js',
                        'js/jquery-ui.min.js',
                        'js/ivo.js',
                        'js/TweenMax.min.js',
                        'js/howler.min.js',
                        'js/main.js'
                  	],
                     complete: init
                  }); // end of yepnope
                  function init() {
                  	main(sym);
                  }

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

})(window.jQuery || AdobeEdge.$, AdobeEdge, "EDGE-103770202");